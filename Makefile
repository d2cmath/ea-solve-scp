# require patch, rm, cp commands

SOURCE = source
PATCH = patch
BUILD = build

all: python3

copy:
	cp $(SOURCE)/solve_scp_libsrc.* $(BUILD)/ 
	cp $(SOURCE)/solve_scp_lib.* $(BUILD)/ 
	cp $(SOURCE)/solve_scp.py $(BUILD)/ 
	cp $(BUILD)/Makefile $(BUILD)/Makefile.in 
	
edit:
	patch $(BUILD)/solve_scp.py -i $(PATCH)/solve_scp.py.python2.patch 
	patch $(BUILD)/Makefile.in -i $(PATCH)/Makefile.python2.patch 

compile:
	make -C $(BUILD) -f Makefile.in all 
	cp $(BUILD)/*.so . 
	cp $(BUILD)/solve_scp.py ./solve_scp 
    	
python3: copy compile
	
python2: copy edit compile
    
clean: 
	rm -f $(BUILD)/*.* solve_scp *.so
	
clean-obj:
	rm -f $(BUILD)/*.o $(BUILD)/*.so solve_scp *.so
	
