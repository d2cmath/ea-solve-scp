Rerun of the AAAI'2021 expriments:

~~~~
  plot.ipynb                Plot the output files with this
  *-output.txt              Raw output files, the columns in order are
                            RANDOM SEED
                            ALGORITHM NAME
                            MUTATION PARAMETER or HYPER-PARAMETER
                            BEST SOLUTION
                            FEASIBILITY OF THE BEST SOLUTION
                            EVALUATION ON WHICH THE BEST SOLUTION IS FOUND
                            TIME TO FIND THE BEST SOLUTION
                            TOTAL TIME

