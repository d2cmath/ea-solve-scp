#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats
import math

from pathlib import Path

# to convert a running tag to the proper algorithm name
tag2algo = {
    "(1+1)-ea"   : r"($1$+$1$) EA", 
    "(1+1)-eah"  : r"($1$+$1$) EA heavy-tailed mut.", 
    "(66+200)-ea": r"($66$+$200$) EA",
    "(66,200)-ea": r"($66$,$200$) EA",
    "0.33-powa"  : r"Power-law rank. ($c=0.33$)",
    "2-lina"     : r"Linear rank. ($\eta=2$)",
    "3-expa"     : r"Exp. rank. ($\eta=3$)",
    "2-tourn"    : r"2-Tournament",
    "3-tourn"    : r"3-Tournament"    
}

# target definition of a successful run (optimal or best-known)
opt = {
    "scpcyc06-output.txt"  :  60,
    "scpcyc07-output.txt"  : 144, # best known
    "scpclr10-output.txt"  :  25, # best known
    "scpclr11-output.txt"  :  23, # best known
    "scpsts81-output.txt"  :  61,
    "scpsts135-output.txt" : 103,
}

# columns of the raw outputfile
colname = ["seed", "algo", "param", "sol", "feas",
           "iter2best", "time2best", "totaltime"]

# confidence interval for bernoulli trials by Clopper-Pearson method
def clopper_pearson(x, n, alpha=0.05):
    lo = scipy.stats.beta.ppf(alpha / 2, x, n - x + 1)
    hi = scipy.stats.beta.ppf(1 - alpha / 2, x + 1, n - x)
    return 0.0 if math.isnan(lo) else lo, 1.0 if math.isnan(hi) else hi
    
# plot the success probabilities for a specific raw output
def plot_succ(frame, target, ax, show_xlabel=True):   
    
    # compute the sucess probabilities
    success, total = {}, {}
    lstAlgo = sorted(pd.unique(frame["algo"]))
    
    for algo in lstAlgo:
        success[algo], total[algo] = {}, {}
        lstParam = sorted(pd.unique(frame.loc[frame["algo"]==algo]["param"]))
        
        for param in lstParam:
            conf = frame.loc[(frame["algo"]==algo) & (frame["param"]==param)]
            stat = conf["sol"].value_counts()
            
            success[algo][param] = stat[target] if target in stat else 0
            total[algo][param] = len(conf.index)    
    
    # make the plot for each algo where its proper name is defined
    for algo in tag2algo:
        if algo in lstAlgo: 
            param = list(success[algo].keys()) 
            prob = [success[algo][p]/total[algo][p] for p in param]
            ci = list(zip(*[clopper_pearson(success[algo][p],total[algo][p]) for p in param]))
            ax.fill_between(param, ci[0], ci[1], alpha=0.1)
            ax.plot(param, prob, label=tag2algo[algo])
    ax.set_ylabel("success prob.")
    if show_xlabel: 
        ax.set_xlabel(r"$\chi$ (or $\beta$)")
    ax.set_xlim(0.1,4.0)    

# MAIN PROGRAM
if __name__ == "__main__":
    
    # setup matplotlib
    plt.rcParams["font.family"] = "serif"
    
    # iterate over the target dictionary and plot if the file exists
    plotted = 0
    for toplot in opt:
        if Path(toplot).is_file():
            # read the raw input
            frame = pd.read_csv(toplot, sep=" ", names=colname)
            
            # prepare a figure and plot
            fig = plt.figure(figsize=(12,4))
            ax = plt.subplot(111) 
            plot_succ(frame, opt[toplot], ax)
            
            # adjust the legend box
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width*0.85, box.height*0.75])
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            
            # save to file 
            plt.savefig(toplot.replace(".txt",".pdf"), bbox_inches='tight')
            plotted += 1

    # make a single figure (sorted by difficulty) if we have everything 
    if plotted==len(opt):    
        # order to plot
        order = ["scpsts81-output.txt", 
                 "scpclr10-output.txt", 
                 "scpcyc06-output.txt", 
                 "scpclr11-output.txt", 
                 "scpcyc07-output.txt",                  
                 "scpsts135-output.txt"] 
        
        # prepare the subfigures
        fig, ax = plt.subplots(len(opt), 1, figsize=(12,3*len(opt))) #sharey=True, sharex=True)              
        for i in range(len(order)):            
            frame = pd.read_csv(order[i], sep=" ", names=colname)
            plot_succ(frame, opt[order[i]], ax[i], True if (i+1)==len(order) else False)
            
            # show the instance name on the right
            twinx = ax[i].twinx()
            twinx.set_yticks([])
            twinx.set_ylabel(order[i].replace("-output.txt",""))
        
        # add the legend
        fig.legend(*(ax[0].get_legend_handles_labels()), 
                   bbox_to_anchor=(0.5, 0.93), loc='upper center', ncol=3)
                
        # save to file 
        plt.savefig("plot-succ-all.pdf")
    
