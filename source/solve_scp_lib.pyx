# distutils: language = c++
# distutils: sources = solve_scp_libsrc.cpp
# cython: language_level = 3

""" EA solver library for set covering 

    providing functions and objects:
     - random_seed, 
     - Data, Evaluator, 
     - BitwiseFlip, BitwiseFlipHT
     - Tournament, Comma
     - PSV, MPL
"""

from libcpp.vector cimport vector

from libcpp cimport bool as bool_t
ctypedef unsigned int uint_t

cimport solve_scp_libsrc as cpp

import gzip

def random_seed(uint_t seed):
    """ change the random seed used by the library's PRNG """

    cpp.random_seed(seed)
    
cdef class Data:
    """ Data input and storage for the SCP 
        
        use Data.read_scp(filename) to read/load input files
    """

    cdef cpp.Data * _cpp
    
    def __cinit__(self):
        """ constructor """
        
        self._cpp = new cpp.Data()
        
    def __dealloc__(self):
        """ destructor """
    
        if self._cpp != NULL:
            del self._cpp
            
    def read_scp(self, fname):
        """ read input file in ORLIB format """
                
        f = gzip.open(fname, "rt") if fname.find(".gz")>0 else open(fname, "rt")
        stream = iter(f.read().split())
        f.close()
        
        m = int(next(stream)) 
        n = int(next(stream))
        w = [next(stream) for i in range(n)]
        
        N = [[] for i in range(n)]        
        for i in range(m):
            nn = int(next(stream))
            for j in range(nn):
                N[int(next(stream))-1].append(i)

        self._cpp.clear()
        self._cpp.set_m(m)
        for ss in N:
            self._cpp.add_subset(ss)
            
    property m:
        def __get__(self): return self._cpp.m    
    property n:
        def __get__(self): return self._cpp.N.size()


cdef class Evaluator:
    """ Evaluator for the algorithms 
        
        construct with Evaluator(data, maxeval, lexicographic)
    """
    
    cdef cpp.Evaluator * _cpp
    
    def __cinit__(self, Data data, uint_t maxeval, bool_t lex=False):
        """ constructor """
        
        self._cpp = new cpp.Evaluator(data._cpp[0], maxeval, lex)
        
    def __dealloc__(self):
        """ destructor """
    
        if self._cpp != NULL:
            del self._cpp
            
    def update(self):
        """ update in case the data has been changed externally """
        
        self._cpp.update()

    property n:
        def __get__(self): return self._cpp.n
    property worstobj:
        def __get__(self): return self._cpp.worstobj[0]+self._cpp.worstobj[1]
    property bestobj:
        def __get__(self): return self._cpp.bestobj[0]+self._cpp.bestobj[1]
    property neval2best:
        def __get__(self): return self._cpp.neval2best
    property bestfeas:
        def __get__(self): return self._cpp.bestobj[0]==0
    property best:
        def __get__(self):
            cdef uint_t n = self._cpp.n
            return [1 if self._cpp.best[i] else 0 for i in range(n)]
    

cdef class Selector: 
    """ Selector for PSV """
    
    cdef cpp.Selector * _cpp

    def __dealloc__(self):
        """ destructor """
        
        if self._cpp != NULL:
            del self._cpp


cdef class Tournament(Selector):
    """ Tournament selection for PSV """
        
    def __cinit__(self, uint_t k):
        """ constructor """
        
        self._cpp = new cpp.Tournament(k)
    
    
cdef class Comma(Selector):
    """ Comma selection for PSV """
    
    def __cinit__(self, uint_t mu):
        """ constructor """
    
        self._cpp = new cpp.Comma(mu)

cdef class LinearAS(Selector):
    """ Linear ranking selection for PSV """
    
    def __cinit__(self, double eta, uint_t la):
        """ constructor """
    
        self._cpp = new cpp.LinearAS(eta, la)

cdef class ExponentialAS(Selector):
    """ Linear ranking selection for PSV """
    
    def __cinit__(self, double eta, uint_t la):
        """ constructor """
    
        self._cpp = new cpp.ExponentialAS(eta, la)

cdef class PowerAS(Selector):
    """ Power-law ranking selection with AS for PSV """
    
    def __cinit__(self, double c, uint_t la):
        """ constructor """
    
        self._cpp = new cpp.PowerAS(c, la)


cdef class CommaITS(Selector):
    """ Comma selection with ITS for PSV """
    
    def __cinit__(self, uint_t mu, uint_t la):
        """ constructor """
    
        self._cpp = new cpp.CommaITS(mu, la)
    

cdef class PowerITS(Selector):
    """ Power-law ranking selection with ITS for PSV """
    
    def __cinit__(self, double c):
        """ constructor """
    
        self._cpp = new cpp.PowerITS(c)

    
cdef class Mutator: 
    """ Mutator for the algorithms """
    
    cdef cpp.Mutator * _cpp
    
    def __dealloc__(self):
        """ destructor """
        
        if self._cpp != NULL:
            del self._cpp
    
    
cdef class BitwiseFlip(Mutator):
    """ Bitwise flip mutation for the algorithms """
    
    def __cinit__(self, double p):
        """ constructor """
        
        self._cpp = new cpp.BitwiseFlip(p)
            

cdef class BitwiseFlipHT(Mutator):
    """ Heavy-tailed bitwise flip mutation for the algorithms """
    
    def __cinit__(self, double beta, uint_t n):
        """ constructor """
        
        self._cpp = new cpp.BitwiseFlipHT(beta, n)


cdef class Algorithm:
    """ Basic algorithm """
    
    cdef cpp.Algorithm * _cpp
    cdef str _name
        
    def start(self, copy=None):
        """ initialize the population """
        
        cdef uint_t i, n = len(copy) if copy is not None else 0
        cdef vector[bool_t] cpp_copy = vector[bool_t](n)
        
        for i in range(n):
            cpp_copy[i] = (copy[i] is True)
            
        self._cpp.start(cpp_copy)
        
    def autorun(self):
        """ execute the algorithm """
    
        self._cpp.autorun()
    
    def __dealloc__(self):
        """ destructor """
        
        if self._cpp != NULL:
            del self._cpp
    
    property name:
        def __get__(self): return self._name
        
    
cdef class PSV(Algorithm):
    """ Population-based selection-variation algorithm 
        
        construct with PSV(evaluator, selector, mutator, population_size)
    """        
    def __cinit__(self, 
                  Evaluator eva, Selector sel, Mutator mut, 
                  uint_t la, str name=""):
        """ constructor """
        
        self._cpp = new cpp.PSV(eva._cpp[0], sel._cpp[0], mut._cpp[0], la)
        self._name = name
    
    
cdef class MPL(Algorithm):
    """ (mu+lambda) evolutionary algorithm
        
        construct with MPL(evaluator, mutator, 
                           offspring_population_size,
                           parent_population_size)
    """
        
    def __cinit__(self, 
                  Evaluator eva, Mutator mut, 
                  uint_t la, uint_t mu, 
                  str name=""):
        """ constructor """
        
        self._cpp = new cpp.MPL(eva._cpp[0], mut._cpp[0], la, mu)
        self._name = name
    
