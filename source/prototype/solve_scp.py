#!/usr/bin/env python3

## written by bibirox7@gmail.com, this software is free and wild
## only require python v3 and numpy package

import numpy as np
from timeit import default_timer as timer
from sys import exit


# -- Utility functions

def exec_and_quit(msg, code, func, *arg):
    """ show the message, execute a function and quit """
    
    print(msg)
    if func: func(*arg)
    exit(code)    
    
def random_bstring(n):
    """ generate a random bitstring of length n """

    return np.random.randint(low=0,high=2,size=n).tolist()
    
def bitwise_flip(s, rate):
    """ create a new bitstring by bitwise flipping s """

    n, ns = len(s), list(s)
    for i in range(n):
        if np.random.uniform()<rate:
            ns[i] = (not ns[i]) + 0
    return ns
    
def bitwise_flip_fast(s, rate):
    """ fast version of bitwise_flip with geometric sampling """    

    n, curr, pos = len(s), np.random.geometric(rate)-1, []
    while curr<n:
        pos.append(curr)        
        curr += np.random.geometric(rate)
    
    ns = list(s)
    for i in pos:
        ns[i] = (not ns[i]) + 0
    return ns

def tournament(fit, k, maxobj=True):
    """ sample an index using tournament selection of size k """

    sel = np.random.randint(low=0, high=len(fit), size=k).tolist()

    selfit = [fit[i] for i in sel]
    selmax = max(selfit) if maxobj else min(selfit)
    return np.random.choice([sel[i] for i,v in enumerate(selfit) 
                             if v==selmax])
    
def tournament_full(fit, k, maxobj=True):
    """ sample len(fit) indices using tournament selection """
    
    return [tournament(fit, k, maxobj) for i in range(len(fit))]

def comma_full(fit, mu, maxobj=True):
    """ sample len(fit) indices using comma selection """
    
    return np.random.choice(sorted(list(range(len(fit))), 
                                   key=lambda i: fit[i], 
                                   reverse=maxobj)[:mu], 
                            size=len(fit)).tolist()


# -- Data and model for the Set Covering Problem

class SCPData:
    """ input data for SCP """
    
    def __init__(self):
        """ constructor """

        self.m=0
        self.n=0
        self.N=[]
        self.w=[] # ignore in this version

    def random(self, m=100, n=100, ss=10):
        """ create a random instance for testing """

        self.m, self.n = m, n
        self.N=[sorted(set(np.random.choice(range(m),size=ss)))
                for i in range(n)]

    def read_scp(self, fname): 
        """ read input file in ORLIB format """
    
        f = gzip.open(fname, "rt") if fname.find(".gz")>0 else open(fname, "rt")
        
        stream = iter(f.read().split())
        f.close()
        
        self.m = int(next(stream)) 
        self.n = int(next(stream))
        
        self.w = [next(stream) for i in range(self.n)]        
        
        self.N = [[] for i in range(self.n)]        
        for i in range(self.m):
            nn = int(next(stream))
            for j in range(nn):
                self.N[int(next(stream))-1].append(i)
                     
class SCPModel:
    """ model SCP solution as bitstring """
    
    def __init__(self, data):
        """ constructor """

        self.data = data
        self.n = data.n
        self.maxobj = False
        self.worstobj = data.m, data.n

    def eval(self, s):
        """ evaluate a bitstring solution 
            return (# uncovered items, # selected set) """
    
        cnt, uncovered = 0, [1]*self.data.m
        for i,v in enumerate(s):
            if v==1:
                cnt += 1
                for j in self.data.N[i]:
                    uncovered[j] = 0
        u = sum(uncovered)
        return u, cnt # +1 penalty for each uncovered item


# -- Evaluator that makes the bridge between the model and algorithms

class Evaluator(SCPModel):
    """ Evaluator that controls the number of evaluations 
        and stores the best solution found so far """
    
    def __init__(self, data, maxeval, lex=False):
        """ constructor """

        super().__init__(data)
        self.maxeval = maxeval
        self.lex = lex # lexicographical objective
        self.reset()

    def expired(self):
        """ check if we have run out of evaluation budget """
        
        return self.neval>=self.maxeval
        
    def reset(self):
        """ reset the evaluator """
    
        self.neval = 0
        self.timerstart = timer()
        self.best = None
        self.bestobj = sum(self.worstobj)
        self.neval2best = 0
        self.time2best = 0
        self.time2expire = 0

    def eval(self, s):
        """ evaluate a bitstring solution 
            if the evaluator is expired, return an useless fitness value """
    
        if self.expired():
            return self.worstobj if self.lex else sum(self.worstobj)
        
        obj = super().eval(s)
        self.neval += 1
        
        if self.neval==self.maxeval:
            self.time2expire = timer()-self.timerstart        
        
        if (obj[0]==0 and
            ((self.maxobj and obj[1]>self.bestobj) 
             or (not self.maxobj and obj[1]<self.bestobj))):
            self.best = s
            self.bestobj = obj[1]
            self.neval2best = self.neval
            self.time2best = timer()-self.timerstart
        
        return obj if self.lex else sum(obj)

        
# -- Algorithms

class BaseAlgorithm:
    """ Base/abtract algorithm """
    
    def __init__(self, model, name=""): 
        """ constructor """
        self.model = model
        self.name = name        

    def start(self): pass # initialize the population
    def run(self): pass   # run one iteration of the algorithm

    def autorun(self): 
        """ iterate the algorithm until the evaluator is expired """
         
        while not self.model.expired():
            self.run()
    
class PSVA(BaseAlgorithm):
    """ Population selection-variation algorithm """
    
    def __init__(self, model, la, sel, mut, name=""):
        """ constructor """
    
        super().__init__(model, name)
        self.la = la
        self.sel = sel
        self.mut = mut
        self.pop = []
        self.fit = []

    def update(self):
        """ update the fitness by evaluating the new population """
        
        self.fit = [self.model.eval(self.pop[i]) 
                    for i in range(self.la)]
        
    def start(self, copy=None):
        """ initialize the population and the evaluator """
    
        self.model.reset()
        if copy is not None:
            self.pop = [list(copy) for i in range(self.la)]
        else:
            self.pop = [random_bstring(self.model.n) for i in range(self.la)]
        self.update()
        
    def run(self):
        """ single iteration by sampling a new population """ 
    
        newpop = [self.mut(self.pop[i]) for i in self.sel(self.fit)]
        self.pop = newpop
        self.update()

class Mupluslambda(BaseAlgorithm):
    """ (mu+lambda) EA """
    
    def __init__(self, model, la, mu, mut, name=""):
        """ constructor """
    
        super().__init__(model, name)
        self.la = la
        self.mu = mu
        self.mut = mut
        self.pop = []
        self.fit = []
        self.mubesti = []
        self.resti = []
        
    def update(self):
        """ re-sort the population and redefine the indices """
        
        psize, mubesti = len(self.pop), set(self.mubesti)
        nov = [0+self.model.maxobj if i not in mubesti else 1-self.model.maxobj
               for i in range(psize)]
        sortedi = sorted(range(psize), 
                         key=lambda i: (self.fit[i], nov[i]), 
                         reverse=self.model.maxobj)
        self.mubesti, self.resti = sortedi[:self.mu], sortedi[self.mu:] 

    def start(self, copy=None):
        """ initialize the population and the evaluator """
        
        self.model.reset()
        if copy is not None:
            self.pop = [list(copy) for i in range(self.mu)]
        else:
            self.pop = [random_bstring(self.model.n) for i in range(self.mu)] 
        
        # create dummy slots for the offspring population
        self.pop = self.pop + [[]]*self.la    
        self.fit = [self.model.eval(self.pop[i]) for i in range(self.mu)] + \
                   [0]*self.la        
        
        self.mubesti, self.resti = list(range(self.mu)), list(range(self.mu, self.mu+self.la))
        
    def run(self):
        """ single iteration by sampling the offspring population """
        
        sel = np.random.choice(self.mubesti, size=len(self.resti)).tolist()
        for i in range(len(self.resti)):
            self.pop[self.resti[i]] = self.mut(self.pop[sel[i]])
            self.fit[self.resti[i]] = self.model.eval(self.pop[self.resti[i]])
        self.update()
    
    
# -- Test & debug

def test():
    np.random.seed(101)

    s = random_bstring(10)
    print(s)
    print(bitwise_flip(s, 0.1))
    print(bitwise_flip_fast(s, 0.1))
    
    print(tournament_full(list(range(10)), k=2))
    print(comma_full(list(range(10)), mu=3))

    data = SCPData()
#    data.random()
    data.read_scp("scpcyc06.txt")
    print(data.m, data.n, data.N[0], data.N[1])
    
    ev = Evaluator(data, maxeval=1)
    s = np.random.randint(low=0, high=2, size=ev.n)
    print(sum(s), ev.eval(s), ev.eval(s))
    
#    np.random.seed(int(timer()))
    
    ev.maxeval = 2000000
    la = 200
#    chi, k = 1.09812, 3
    chi, k = 1.08, 3

    algo = PSVA(ev, la, 
#                sel=lambda fit: comma_full(fit, la//3, ev.maxobj),
                sel=lambda fit: tournament_full(fit, k, ev.maxobj),
                mut=lambda s: bitwise_flip_fast(s, chi/ev.n))

#    algo = Mupluslambda(ev, 1, 1, 
#                        mut=lambda s: bitwise_flip_fast(s, chi/ev.n))

#    algo = Mupluslambda(ev, la, la//3, 
#                        mut=lambda s: bitwise_flip_fast(s, chi/ev.n))

    algo.start([0]*ev.n)
#    algo.start()
    algo.autorun()
    print("objective={}\n"
          " covered={}\n"
          " time to best={:.3f}s\n"
          " evaluations to best={}\n"
          " total time={:.3f}s".format(
           ev.bestobj,
           sum(ev.best)==ev.bestobj,
           ev.time2best,
           ev.neval2best,
           ev.time2expire))
    
    exit(-1)    


# -- The main program

if __name__=="__main__":    
    
#    test()

    from argparse import ArgumentParser 

    parser = ArgumentParser(description="Pure EA solvers for the set covering problem with unicost. "
                                        "Supported algorithms are non-elitist EAs with tournament "
                                        "or comma selection, and the (mu+lambda) EA. The output format "
                                        "is: [Tag] [BestObj] [NumEvalToBest] [TimeToBest] [TotalTime], "
                                        "here BestObj is None if no feasible solution is found by "
                                        "the selected solver. This software is free and wild!")
        
    parser.add_argument("-i", "--input", help="input data file in ORLIB format")
    parser.add_argument("-r", "--run", help="algorithm to run: tourn (default), comma, or plus")    
    parser.add_argument("-t", "--tag", help="running tag to show with the output (default contains the parameters)")

    parser.add_argument("-l", "--la", type=int, help="offspring population size (default=200)")
    parser.add_argument("-m", "--mu", type=int, help="parent population size (default=66)")
    parser.add_argument("-k", "--tsize", type=int, help="tournament size of for tourn-algorithm (default=3)")
    parser.add_argument("-x", "--chi", type=float, help="parameter chi of the mutation rate (default=1.08)")

    parser.add_argument("-b", "--start", type=int, help="start the population at a fixed number of leading ones (if this is specified)")
    parser.add_argument("-e", "--maxeval", type=float, help="maximal number of evaluations allowed (default=2e5)")
    
    parser.add_argument("-o", "--lexic", action="store_true", help="enable lexicographical objective (if this is set)")
    parser.add_argument("-s", "--seed", type=int, help="use a specific random seed (if this is specified)")
    
    args = parser.parse_args()
    
    la = args.la if args.la is not None else 200
    mu = args.mu if args.mu is not None else 66
    tsize = args.tsize if args.tsize is not None else 3
    chi = args.chi if args.chi is not None else 1.08    
    maxeval = int(args.maxeval) if args.maxeval is not None else 200000

    if args.input is None:
        exec_and_quit("ERR: no input file was specified, please check the usage below!\n", 
                      -1, parser.print_help)

    if args.run is not None and args.run not in ["tourn","comma","plus"]:
        exec_and_quit("ERR: unknown alorithm, please check the usage below!\n", 
                      -1, parser.print_help)    
                      
    if args.tag is None:
        tag = "{}_{}_l{}m{}k{}x{:.2f}".format(args.input, 
                args.run if args.run is not None else "tourn",
                la, mu, tsize, chi)
        tag += "_s{}".format(args.seed) if args.seed is not None else ""
    else:
        tag = args.tag
             
    if args.seed is not None:
        np.random.seed(args.seed)
                      
    data = SCPData()
    data.read_scp(args.input)
        
    ev = Evaluator(data, maxeval, lex=args.lexic) 
    
    algo = PSVA(ev, la, 
                sel=lambda fit: tournament_full(fit, tsize, ev.maxobj),
                mut=lambda s: bitwise_flip_fast(s, chi/ev.n))
    if args.run=="comma":
        algo = PSVA(ev, la, 
                    sel=lambda fit: comma_full(fit, mu, ev.maxobj),
                    mut=lambda s: bitwise_flip_fast(s, chi/ev.n))
    elif args.run=="plus":
        algo = Mupluslambda(ev, la, mu, 
                            mut=lambda s: bitwise_flip_fast(s, chi/ev.n))
    
    if args.start is not None:
        algo.start([1]*args.start + [0]*(ev.n - args.start))
    else:
        algo.start()        
    algo.autorun()
    
    print("{} {} {} {:.3f} {:.3f}".format(tag, 
                         ev.bestobj if ev.best is not None else None,
                         ev.neval2best, 
                         ev.time2best,
                         ev.time2expire))
    
