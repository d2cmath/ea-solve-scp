#include "solve_scp_libsrc.h"

#include <stack>

#include <chrono>
#include <random>
#include <algorithm>

#include <numeric>

/* =====================================
    Internal data and utility functions 
   ===================================== */

/* generator of random number */
mt19937_64 PRNG = mt19937_64(chrono::steady_clock::now().time_since_epoch().count());

/* uniform distribution over [0,1) */
uniform_real_distribution<double> UNIF01(0, 1.);

/* change the random seed of the generator */
void random_seed(unsigned int value) {
    PRNG.seed(value);
}

/* create a random bitstring of length n */
vector<bool> random_bstring(unsigned int n) {
    vector<bool> res(n);
    
    for (unsigned int i=0; i<n; ++i) {
        res[i] = (UNIF01(PRNG)<0.5);
    }
    
    return res;
}

/* sample with alias method */
alias_sampler::alias_sampler(): n(0) {}
alias_sampler::alias_sampler(const vector<double> & probmass) {
    this->setup(probmass);
}

void alias_sampler::setup(const vector<double> & probmass) { 
    
    this->n = probmass.size();
    this->prob.resize(n);
    this->alias.resize(n);
    
    unsigned int a, b;
    stack<unsigned int> above, below;
    
    for (unsigned int i=0; i<this->n; i++) { // find above and below 1. masses
        this->prob[i] = this->n*probmass[i];
        this->alias[i] = -1;
        if (this->prob[i]>1.) {
            above.push(i);
        } else if (this->prob[i]<1.) {
            below.push(i);
        }
    }
    
    while (!above.empty() && !below.empty()) { // redistribute the masses
        a = above.top(), b = below.top();         
        
        this->alias[b] = a; below.pop();        
        this->prob[a] = (this->prob[a] + this->prob[b]) - 1.;
        
        if (this->prob[a]<1.) {
            above.pop();
            below.push(a);
        } else if (this->prob[a]==1.) {
            above.pop();
        }
    }
}

template<class Generator>
unsigned int alias_sampler::operator()(Generator & g) const {
    double u = UNIF01(g);
    unsigned int ri = floor(u*n); 
    
    //return (this->alias[ri]==-1 || UNIF01(g)<this->prob[ri])?ri:alias[ri]; // standard version
    return (this->alias[ri]==-1 || (u*this->n - ri)<this->prob[ri])?ri:alias[ri]; // version that re-uses the sampled number
}

/* pick elements with respect to ranks */
template<class DataType> 
rank_sampler::rank_sampler(const vector<DataType> & input, bool maximize):
    n(input.size()) {

    // create and sort the indices based on the input
    vector<unsigned int> idx(this->n);
    for (unsigned int i=0; i<this->n; ++i) idx[i]=i; 
    
    sort(idx.begin(), idx.end(),
         [maximize, &input](unsigned int i1, unsigned int i2){
            return (maximize && input[i2]<input[i1])
                    || (not maximize && input[i1]<input[i2]);
    });
    
    // put them in buckets
    for (unsigned int i=0; i<this->n; ++i) {
        if (!this->bidx.empty() && input[idx[i]]==input[idx[i-1]]){
            this->bidx.back().push_back(idx[i]);
        } else {
            this->bidx.push_back(vector<unsigned int>{idx[i]});
        }
    }
    
    // point each rank to the appropriate bucket
    for (unsigned int i=0; i<this->bidx.size(); ++i) {
        for (unsigned int j=0; j<this->bidx[i].size(); ++j) {
            this->pbidx.push_back(&(this->bidx[i]));
        }
    }    
}

template<class Generator> 
unsigned int rank_sampler::operator()(double gamma, Generator & g) const {
    unsigned int rank = floor(gamma*this->n);
    const vector<unsigned int> * bucket = this->pbidx[rank];
    unsigned int s = bucket->size();
    
    if (s==1) return (*bucket)[0];
    
    return (*bucket)[floor(UNIF01(g)*s)];
}


/* =====================================
            Implementation of 
         Data and model for the SCP 
   ===================================== */

/* Data class */
SCP::Data::Data():
    m(0) { 
}   
   
void SCP::Data::clear() {
    this->m = 0;
    this->N.clear();
}

void SCP::Data::set_m(unsigned int m) {
    this->m = m;
}

void SCP::Data::add_subset(const std::vector< unsigned int > & ss) {
    this->N.push_back(ss);
}

/* Model class */
SCP::Model::Model(const SCP::Data & data): 
    data(data), 
    maximize(false),
    n(data.N.size()), 
    worstobj({data.m, n}) {
}

void SCP::Model::update() {
    n = data.N.size();
    worstobj = vector< unsigned int >({data.m, n});
}

vector< unsigned int > SCP::Model::eval_string(const vector< bool > & s) const {
    unsigned int m = this->data.m;
    vector<bool> u(m, 1);
    vector<unsigned int> res(2, 0);    
    
    for (unsigned int i=0; i<s.size(); ++i) {
        if (s[i]) {
            res[1]++;
            for (unsigned int j=0; j<this->data.N[i].size(); ++j) {
                u[this->data.N[i][j]] = 0;
            }
        }
    }
    
    for (unsigned int i=0; i<m; ++i) {
        res[0] += u[i];
    }
    
    return res;
}


/* =====================================
            Implementation of 
               EAs for SCP
   ===================================== */

/* Objective */
EA::ObjType::ObjType():
    lex(false), 
    raw(2,0), 
    agg(0) { 
}

EA::ObjType::ObjType(bool lex, const vector<unsigned int> & raw):
    lex(lex), 
    raw(raw), 
    agg(raw[0]+raw[1]) { 
}

bool EA::ObjType::operator<(const ObjType & b) const {
    if (not this->lex) {
        return this->agg < b.agg;
    }
    
    return (this->raw[0]<b.raw[0] || 
            (this->raw[0]==b.raw[0] && this->raw[1]<b.raw[1]));
}

bool EA::ObjType::operator>(const ObjType & b) const {
    if (not this->lex) {
        return this->agg > b.agg;
    }
    
    return (this->raw[0]>b.raw[0] || 
            (this->raw[0]==b.raw[0] && this->raw[1]>b.raw[1]));
}

bool EA::ObjType::operator==(const ObjType & b) const {
    if (not this->lex) {
        return this->agg==b.agg;
    }
    
    return (this->raw[0]==b.raw[0] && this->raw[1]==b.raw[1]);
}

/* Evaluator */
EA::Evaluator::Evaluator(const SCP::Data & data, unsigned int maxeval, bool lex):
    SCP::Model(data), 
    maxeval(maxeval), neval(0), 
    lex(lex), 
    bestobj(worstobj), 
    neval2best(0) {
}

void EA::Evaluator::reset() {
    this->neval = 0;
    this->best.clear();
    this->bestobj = worstobj;
    this->neval2best = 0;
}

bool EA::Evaluator::expired() const {
    return (this->neval >= this->maxeval);
}

EA::ObjType EA::Evaluator::operator()(const vector< bool > & s) {
    if (this->expired()) {
        return ObjType(this->lex, this->worstobj);
    }
    
    ObjType obj(this->lex, this->eval_string(s));    
    this->neval++;
    
    if ((this->maximize && obj>ObjType(this->lex, this->bestobj))
         || (not this->maximize && obj<ObjType(this->lex, this->bestobj))) {        
        this->best = s;
        this->bestobj = obj.raw;
        this->neval2best = this->neval;        
    }
    
    return obj;
}

/* Selection operators */
EA::Tournament::Tournament(unsigned int k):
    k(k) {    
}

vector<unsigned int> EA::Tournament::operator()(const vector<ObjType> & fit, 
                                                bool maximize) const {
    unsigned int n = fit.size();
    vector<unsigned int> res(n);    
    
    unsigned int sel, next; ObjType best;
    
    for (unsigned int i=0; i<n; ++i) {        
        sel = next = floor(UNIF01(PRNG)*n); best = fit[next];
        for (unsigned int j=1; j<this->k; ++j) {
             next = floor(UNIF01(PRNG)*n);
             
             if ((maximize && best<fit[next])
                 || (not maximize && fit[next]<best)) {
                sel = next;
                best = fit[next];
             }
        }        
        res[i] = sel;
    }
    
    return res;
}

EA::Comma::Comma(unsigned int mu):
    mu(mu) {
}

vector<unsigned int> EA::Comma::operator()(const vector<ObjType> & fit, 
                                           bool maximize) const {
    unsigned int n = fit.size();
    vector<unsigned int> res(n);
    
    vector<unsigned int> idx(n);
    for (unsigned int i=0; i<n; ++i) idx[i] = i;
    
    sort(idx.begin(), idx.end(), 
         [maximize, &fit](unsigned int i1, unsigned int i2){ 
            return (maximize && fit[i2]<fit[i1]) 
                    || (not maximize && fit[i1]<fit[i2]); 
         }); 
    
    for (unsigned int i=0; i<n; ++i) {
        res[i] = idx[floor(UNIF01(PRNG)*this->mu)];
    }
    
    return res;
}

vector<unsigned int> EA::SelectorAS::operator()(const vector<ObjType> & fit, 
                                             bool maximize) const {
    unsigned int n = fit.size(); // we need n==lambda
    vector<unsigned int> res(n);
        
    rank_sampler sampler(fit, maximize);
    vector<double> gamma(n);
    for (unsigned int i=0; i<n; ++i) gamma[i] = double(this->rankgen(PRNG))/n;    
    for (unsigned int i=0; i<n; ++i) res[i] = sampler(gamma[i], PRNG);
    
    return res;
}

EA::LinearAS::LinearAS(double eta, unsigned int lambda): eta(eta) { 
    vector<double> prob(lambda);
    double sum = 0, gamma; for (unsigned int i=0; i<lambda; ++i) {
        gamma = double(i)/(lambda-1);
        prob[i] = this->eta*(1-2*gamma)+2*gamma; // note: if eta=2, the worst individual is discarded
        sum += prob[i];
    }
    for (unsigned int i=0; i<lambda; ++i) prob[i] /= sum;
    
    this->rankgen.setup(prob);
}

EA::ExponentialAS::ExponentialAS(double eta, unsigned int lambda): eta(eta) { 
    vector<double> prob(lambda);
    double sum = 0, gamma; for (unsigned int i=0; i<lambda; ++i) {
        gamma = double(i)/(lambda-1);    
        prob[i] = this->eta*exp(this->eta*(1-gamma))/(exp(this->eta)-1);
        sum += prob[i];
    }
    for (unsigned int i=0; i<lambda; ++i) prob[i] /= sum;
    
    this->rankgen.setup(prob);
}

EA::PowerAS::PowerAS(double c, unsigned int lambda): c(c) { 
    vector<double> prob(lambda);
    double sum = 0; for (unsigned int i=0; i<lambda; ++i) {
        prob[i] = pow(double(i+1)/lambda, c) - pow(double(i)/lambda, c);
        sum += prob[i];
    }
    for (unsigned int i=0; i<lambda; ++i) prob[i] /= sum;
    
    this->rankgen.setup(prob);
}

vector<unsigned int> EA::SelectorITS::operator()(const vector<ObjType> & fit, 
                                                 bool maximize) const {
    unsigned int n = fit.size();
    vector<unsigned int> res(n);
        
    rank_sampler sampler(fit, maximize);

    vector<double> gamma(n);
    for (unsigned int i=0; i<n; ++i) gamma[i] = this->gamma();    
    for (unsigned int i=0; i<n; ++i) res[i] = sampler(gamma[i], PRNG);
    
    return res;
}

EA::CommaITS::CommaITS(unsigned int mu, unsigned int lambda):
    ratio(double(mu)/lambda) {
}

double EA::CommaITS::gamma() const {
    return UNIF01(PRNG)*this->ratio; 
}

EA::PowerITS::PowerITS(double c):
    invc(1./c) {
}

double EA::PowerITS::gamma() const {
    return pow(UNIF01(PRNG), invc); 
}


/* Variation operators */
EA::BitwiseFlip::BitwiseFlip(double p): 
    p(p) {
}

double EA::BitwiseFlip::prob() const {
    return this->p; 
}
    
vector<bool> EA::BitwiseFlip::operator()(const vector<bool> & s) const {
    // fast bitwise flip with geometric sampling 
    
    unsigned int n = s.size(), curr;
    vector<bool> res = s;
    
    geometric_distribution<unsigned int> geo(this->prob()); 
    curr = geo(PRNG);
    
    while (curr<n) {
        res[curr] = not res[curr];
        curr += (1+geo(PRNG));
    }
    
    return res;
}

EA::BitwiseFlipHT::BitwiseFlipHT(double beta, unsigned int n): 
    BitwiseFlip(0), beta(beta), n(n) {
    
    vector<double> p(this->n/2);
    
    double sum = 0.;    
    for (unsigned int i=0; i<p.size(); ++i) {
        p[i] = pow(i+1, -this->beta);
        sum += p[i];
    }    
    for (unsigned int i=0; i<p.size(); ++i) p[i] /= sum;
        
    this->gennum.setup(p);
}

double EA::BitwiseFlipHT::prob() const {
    return double(this->gennum(PRNG))/n; 
}    


/* Abstract algorithm */
EA::Algorithm::Algorithm(EA::Evaluator & eval): 
    eval(eval) {
}

void EA::Algorithm::autorun() {
    while (not this->eval.expired()) {
        this->run();
    }
}

/* Population-based selection-variation algorithm */ 
EA::PSV::PSV(EA::Evaluator & eval, 
             EA::Selector & sel, 
             EA::Mutator & mut, 
             unsigned int la): 
    EA::Algorithm(eval), 
    sel(sel), mut(mut),
    la(la) {
}

void EA::PSV::update() {
    for (unsigned int i=0; i<this->la; ++i) {
        this->fit[i] = this->eval(this->pop[i]); 
    }
}
    
void EA::PSV::start(const vector<bool> & copy) {
    this->eval.reset();
        
    this->pop = vector<vector<bool>>(this->la);
    this->fit = vector<EA::ObjType>(this->la);
    
    for (unsigned int i=0; i<this->la; ++i) {
        this->pop[i] = copy.size()>0?copy:random_bstring(this->eval.n);
    }
    
    this->update();
}
    
void EA::PSV::run() {
    vector<vector<bool>> newpop(this->la);
    vector<unsigned int> selected = this->sel(this->fit, this->eval.maximize);
    
    for (unsigned int i=0; i<this->la; ++i) {
        newpop[i] = this->mut(this->pop[selected[i]]);
    }
    
    this->pop = newpop;
    this->update();
} 

/* The (mu+lambda) evolutionary algorithm */
EA::MPL::MPL(EA::Evaluator & eval, 
             EA::Mutator & mut, 
             unsigned int la, 
             unsigned int mu):
    EA::Algorithm(eval), 
    mut(mut), 
    la(la), mu(mu) {
}

void EA::MPL::update() {
    bool maximize = this->eval.maximize;
    const vector<ObjType> & fit = this->fit;

    vector<unsigned int> idx(this->mu + this->la);
    for (unsigned int i=0; i<this->mu + this->la; ++i) idx[i] = i;
    
    vector<bool> nov(this->mu + this->la); // novelty marker for the offspring
    for (unsigned int i=0; i<this->mu; ++i) nov[this->mubesti[i]] = not maximize;
    for (unsigned int i=0; i<this->la; ++i) nov[this->resti[i]] = maximize;
    
    sort(idx.begin(), idx.end(),
         [&maximize, &fit, &nov](unsigned int i1, unsigned int i2){
            return    (maximize     && (fit[i2]<fit[i1] 
                                        || (fit[i2]==fit[i1] && nov[i2]<nov[i1])))
                   || (not maximize && (fit[i1]<fit[i2] 
                                        || (fit[i1]==fit[i2] && nov[i1]<nov[i2])));
         });
    
    for (unsigned int i=0; i<this->mu; ++i) this->mubesti[i] = idx[i];
    for (unsigned int i=0; i<this->la; ++i) this->resti[i] = idx[this->mu + i];
}

void EA::MPL::start(const vector<bool> & copy) {
    this->eval.reset();
    
    this->pop = vector<vector<bool>>(this->mu + this->la);
    this->fit = vector<EA::ObjType>(this->mu + this->la);
    
    for (unsigned int i=0; i<this->mu; ++i) {
        this->pop[i] = copy.size()>0?copy:random_bstring(this->eval.n);
        this->fit[i] = this->eval(this->pop[i]);
    }
    
    this->mubesti = vector<unsigned int>(this->mu);
    this->resti = vector<unsigned int>(this->la);
    
    for (unsigned int i=0; i<this->mu; ++i) this->mubesti[i] = i;    
    for (unsigned int i=0; i<this->la; ++i) this->resti[i] = this->mu + i;
}

void EA::MPL::run() {
    unsigned int seli, newi;
    
    for (unsigned int i=0; i<this->la; ++i) {
        seli = this->mubesti[floor(UNIF01(PRNG)*this->mu)];
        newi = this->resti[i];
        this->pop[newi] = this->mut(this->pop[seli]);
        this->fit[newi] = this->eval(this->pop[newi]);
    }
    
    this->update();
}
