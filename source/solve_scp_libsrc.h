#ifndef SOLVE_SCP_LIBSRC_H
#define SOLVE_SCP_LIBSRC_H

#include <vector>

using namespace std;

/** Change the random seed of the PRNG
  * @param value the new random seed
  */
void random_seed(unsigned int value);


/** Sample discrete distribution with alias method, 
    this is faster than random::discrete_distribution */
class alias_sampler {
    unsigned int n;
    vector<double> prob; // probability of the current slot
    vector<int> alias; // alias table, a -1 means no external alias (a full slot)
public:     
    
    /** default constructor */
    alias_sampler();

    /** constructor that has the distribution as input */
    alias_sampler(const vector<double> & probmass); 
    
    /** setup the sampler */
    void setup(const vector<double> & probmass); 

    /* sample with operator(engine) like other distributions of random */
    template<class Generator> 
    unsigned int operator()(Generator & g) const; 
};

/** Sample or pick index according to rank, 
    thus equally ranked elements have the same chance */
class rank_sampler {
    unsigned int n; 
    vector< vector<unsigned int> > bidx; // ordered indices by buckets of the same rank
    vector< vector<unsigned int> * > pbidx; // pointers to the buckets
public:

    /** constructor */
    template<class DataType>
    rank_sampler(const vector<DataType> & input, bool maximize=false);
    
    /** pick an index of gamma-ranked */ 
    template<class Generator> 
    unsigned int operator()(double gamma, Generator & g) const; 
};


/** Data and model for the Set Covering Problem (SCP) */
namespace SCP {

/** Data of SCP in forms of family of subsets */ 
class Data {
public:
    unsigned int m;
    vector<vector<unsigned int>> N;
    
    /** constructor */
    Data();
    
    /** empty the structure */ 
    void clear(); 
    
    /** set the size of the universal set */
    void set_m(unsigned int m); 
    
    /** add a new subset */
    void add_subset(const vector<unsigned int> & ss); 
};

/** Model of the solutions of SCP as bitstrings */ 
class Model {
public: 
    const Data & data;
    bool maximize;
    unsigned int n;
    vector<unsigned int> worstobj;
    
    /** constructor */
    Model(const Data & data);
    
    /** update the model in case the data has been changed externally */
    void update();
    
    /** evaluate a bitstring solution 
      * @param s a string of length n 
      * @return [number of uncovered elements, number of selected subsets] 
      */
    vector<unsigned int> eval_string(const vector<bool> & s) const;
};

};


/** Evolutionary Aglrithms (EAs) on bitstring search space binding to SCP */
namespace EA {

/** Objective */
class ObjType {
public:
    bool lex;
    vector<unsigned int> raw;
    unsigned int agg;
    
    /** default constructor */
    ObjType();
    
    /** constructor */
    ObjType(bool lex, const vector<unsigned int> & raw);
    
    /** implementation of less than operator */
    bool operator<(const ObjType & b) const;

    /** implementation of greater than operator */
    bool operator>(const ObjType & b) const;
    
    /** implementation of equal comparison */
    bool operator==(const ObjType & b) const;
}; 

/** Evaluation operator */
class Evaluator : public SCP::Model {
public:
    unsigned int maxeval;
    unsigned int neval;
    bool lex;
    vector<bool> best;
    vector<unsigned int> bestobj;
    unsigned int neval2best;
    
    /** constructor */
    Evaluator(const SCP::Data & data, unsigned int maxeval, bool lex=false); 
    
    /** reset evaluation counting */
    void reset(); 
    
    /** check if the budget of number of evaluations has been exceeded */
    bool expired() const; 
    
    /** evaluate a bitstring */
    ObjType operator()(const vector<bool> & s); 
};

/** Abtract selection operator */
class Selector {
public: 
    
    /** select indices based on fitness
      * @param fit a vector of ObjType 
      * @param maximize a boolean which is true for a maximization problem
      * @return a vector of fit.size() selected indices 
      */
    virtual vector<unsigned int> operator()(const vector<ObjType> & fit,
                                            bool maximize=false) const = 0;
    
    virtual ~Selector() { /* only to avoid compilation warning */ };
};

/** Tournament selection operator */
class Tournament : public Selector {
public:
    unsigned int k;
    
    /** constructor */
    Tournament(unsigned int k);
    
    /** select indices */
    virtual vector<unsigned int> operator()(const vector<ObjType> & fit, 
                                            bool maximize=false) const;
};

/** Comma selection operator */
class Comma : public Selector {
public:
    unsigned int mu;
    
    /** constructor */
    Comma(unsigned int mu);
    
    /** select indices */
    virtual vector<unsigned int> operator()(const vector<ObjType> & fit, 
                                            bool maximize=false) const;
};

/** Abstract selector using alias sampling */ 
class SelectorAS : public Selector {
protected:
    alias_sampler rankgen;
    
    /** protected constructor, this makes the class abstract */
    SelectorAS() {}; 
public:             
    /** select indices */
    virtual vector<unsigned int> operator()(const vector<ObjType> & fit, 
                                            bool maximize=false) const;
                                            
    virtual ~SelectorAS() { /* only to avoid compilation warning */ };                                            
};

/** Linear-ranking selection using alias sampling */
class LinearAS : public SelectorAS {
public:
    double eta; // this must be between [1.0,2.0]
    
    /** constructor */
    LinearAS(double eta, unsigned int lambda);
};

/** Exponential-ranking selection using alias sampling */
class ExponentialAS : public SelectorAS {
public:
    double eta;
    
    /** constructor */
    ExponentialAS(double eta, unsigned int lambda);
};

/** Power-law ranking operator using alias sampling */
class PowerAS : public SelectorAS {
public:
    double c;
    
    /** constructor */
    PowerAS(double c, unsigned int lambda);
};

/** Abstract selector using inverse transform sampling */ 
class SelectorITS : public Selector {
public: 
    
    /** sample a gamma-rank using the inverse transform of the cdf */
    virtual double gamma() const = 0;
    
    /** select indices by sampling gamma-ranks */
    virtual vector<unsigned int> operator()(const vector<ObjType> & fit, 
                                            bool maximize=false) const;
    
    virtual ~SelectorITS() { /* only to avoid compilation warning */ };
};


/** Comma selection operator using inverse transform sampling,
    this has more stochasticity in a single generation */
class CommaITS : public SelectorITS {
public:
    double ratio;
    
    /** constructor */
    CommaITS(unsigned int mu, unsigned int la);
    
    /** sample a gamma-rank */
    virtual double gamma() const;
};

/** Power-law ranking selection operator using inverse transform sampling */
class PowerITS : public SelectorITS {
public:
    double invc;
    
    /** constructor */
    PowerITS(double c);
    
    /** sample a gamma-rank */
    virtual double gamma() const;
};



/** Abtract mutation operator */
class Mutator {
public:
    /** create a new bitstring by copying and modifying the input string
      * @param s an input bitstring
      * @return a new bitstring
      */
    virtual vector<bool> operator()(const vector<bool> & s) const = 0;
    
    virtual ~Mutator() { /* only to avoid compilation warning */ };
};

/** Bitwise flip mutation operator */
class BitwiseFlip : public Mutator {
    double p;
public:    
    /** constructor */
    BitwiseFlip(double p);
    
    /** get the current mutation probability */
    virtual double prob() const; 
    
    /** bitwise flip the input to produce the output */
    virtual vector<bool> operator()(const vector<bool> & s) const;    
};

/** Heavy-tailed bitwise flip mutation operator */
class BitwiseFlipHT : public BitwiseFlip {
    double beta;
    unsigned int n;
    alias_sampler gennum;
public:
    
    /** constructor */
    BitwiseFlipHT(double beta, unsigned int n);
    
    /** get the current mutation probability */
    virtual double prob() const; 
};


/** Abstract algorithm */
class Algorithm {
public:
    Evaluator & eval;
    
    /** constructor */
    Algorithm(Evaluator & eval);
    
    /** initialize the population */
    virtual void start(const vector<bool> & copy = vector<bool>()) = 0;
    
    /** execute one iteration of the algorithm */
    virtual void run() = 0;
    
    /** execute the algorithm until the expiration of the evaluator */
    virtual void autorun(); 
    
    virtual ~Algorithm() { /* only to avoid compilation warning */ };
};

/** Population-based selection-variation algorithm */
class PSV : public Algorithm {
public:
    Selector & sel;
    Mutator & mut;
    unsigned int la; 
    vector<vector<bool>> pop;
    vector<ObjType> fit;
    
    /** constructor 
      * @param eval an evaluator
      * @param sel a selector
      * @param mut a mutator
      * @param la the population size
      */
    PSV(Evaluator & eval, Selector & sel, Mutator & mut, unsigned int la);
    
    /** update the fitness by evaluating the current population */
    void update();
    
    /** initialize the population and the evaluator */
    virtual void start(const vector<bool> & copy = vector<bool>());
    
    /** execute one iteration of the algorithm */
    virtual void run();
};

/** (mu+lambda) evolutionary algorithm */
class MPL : public Algorithm {
public:
    Mutator & mut;
    unsigned int la;
    unsigned int mu;
    vector<vector<bool>> pop;
    vector<ObjType> fit;
    vector<unsigned int> mubesti;
    vector<unsigned int> resti;
    
    /** constructor
      * @param eval an evaluator
      * @param mut a mutator
      * @param la the offspring population size
      * @paran mu the parent population size
      */
    MPL(Evaluator & eval, Mutator & mut, unsigned int la, unsigned int mu);
    
    /** re-sort the population and re-compute the indices */
    void update();
    
    /** initialize the population and the evaluator */
    virtual void start(const vector<bool> & copy = vector<bool>());

    /** execute one iteration of the algorithm */
    virtual void run();    
};

};

#endif
