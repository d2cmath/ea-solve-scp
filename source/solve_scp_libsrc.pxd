from libcpp.vector cimport vector

from libcpp cimport bool as bool_t
ctypedef unsigned int uint_t

cdef extern from "solve_scp_libsrc.h":
    void random_seed(uint_t value)
    
cdef extern from "solve_scp_libsrc.h" namespace "SCP":
    cdef cppclass Data:
        uint_t m
        vector[vector[uint_t]] N
        
        void clear()
        void set_m(uint_t m)
        void add_subset(const vector[uint_t] & ss)

cdef extern from "solve_scp_libsrc.h" namespace "EA":
    cdef cppclass Evaluator:
        vector[uint_t] worstobj
        uint_t n
        vector[bool_t] best
        vector[uint_t] bestobj
        uint_t neval2best
    
        Evaluator(const Data & data, uint_t maxeval, bool_t lex)
        void update()
    
    cdef cppclass Selector: 
        pass
    
    cdef cppclass Tournament(Selector):
        Tournament(uint_t k)
        
    cdef cppclass Comma(Selector):
        Comma(uint_t mu)

    cdef cppclass LinearAS(Selector):
        LinearAS(double eta, uint_t la)

    cdef cppclass ExponentialAS(Selector):
        ExponentialAS(double eta, uint_t la)
        
    cdef cppclass PowerAS(Selector):
        PowerAS(double c, uint_t la)

    cdef cppclass CommaITS(Selector):
        CommaITS(uint_t mu, uint_t la)

    cdef cppclass PowerITS(Selector):
        PowerITS(double c)
        
    cdef cppclass Mutator:
        pass
        
    cdef cppclass BitwiseFlip(Mutator):
        BitwiseFlip(double p)

    cdef cppclass BitwiseFlipHT(Mutator):
        BitwiseFlipHT(double beta, uint_t n)
    
    cdef cppclass Algorithm:
        void autorun()
        void start(const vector[bool_t] & s)
    
    cdef cppclass PSV(Algorithm):
        PSV(Evaluator & eva, Selector & sel, Mutator & mut, uint_t la)
        
    cdef cppclass MPL(Algorithm):
        MPL(Evaluator & eva, Mutator & mut, uint_t la, uint_t mu)
    

