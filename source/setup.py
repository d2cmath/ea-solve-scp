from setuptools import setup
from Cython.Build import cythonize

setup(name="solve_scp_lib", 
      description="EA solver library for set covering", 
      version="1.0", 
      author="bibirox7@gmail.com", 
      license="Beerware", 
      ext_modules=cythonize("solve_scp_lib.pyx", 
                            annotate=True))
