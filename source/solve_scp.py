#!/usr/bin/env python3

## written by bibirox7@gmail.com, this software is free and wild

from timeit import default_timer as timer
from sys import exit

from solve_scp_lib import random_seed, \
                          Data, Evaluator, \
                          Tournament, Comma, \
                          LinearAS, ExponentialAS, PowerAS, \
                          CommaITS, PowerITS, \
                          BitwiseFlip, BitwiseFlipHT, \
                          PSV, MPL

# -- Utility functions

def exec_and_quit(msg, code, func, *arg):
    """ show the message, execute a function and quit """
    
    print(msg)
    if func: func(*arg)
    exit(code)    
    
# -- The main program

if __name__=="__main__":    
    
#    test()

    from argparse import ArgumentParser 

    parser = ArgumentParser(description="Pure EA solvers for the set covering problem with unicost. "
                                        "Supported algorithms are non-elitist EAs with tournament, "
                                        "comma, (linear, exponential or power-law) ranking selections, " 
                                        "and the (mu+lambda) EA. The output format is: "
                                        "[Tag] [BestObj] [Feasibility] [NumEvalToBest] [TimeToBest] [TotalTime]. "
                                        "This software is free and wild!")
        
    parser.add_argument("-i", "--input", help="input data file in ORLIB format")
    parser.add_argument("-r", "--run", default="tourn", help="algorithm to run: tourn (default), comma, lina, expa, powa, commai, powi or plus")
    parser.add_argument("-u", "--use", default="fixed", help="mutation to use: fixed (default), or heavy")    
    parser.add_argument("-t", "--tag", help="running tag to show with the output (default contains the parameters)")

    parser.add_argument("-l", "--la",    type=int,   default=200, help="offspring population size (default=200)")
    parser.add_argument("-m", "--mu",    type=int,   default=66, help="parent population size (default=66)")
    parser.add_argument("-k", "--tsize", type=int,   default=3, help="tournament size of tourn-algorithm (default=3)")
    parser.add_argument("-n", "--eta",   type=float, default=2.0, help="selection pressure of (lin and exp)-algorithms (default=2.0)")    
    parser.add_argument("-c", "--expo",  type=float, default=0.33, help="exponent c of pow-algorithm (default=0.33)") 
    parser.add_argument("-x", "--chi",   type=float, default=1.08, help="parameter chi of the fixed mutation (default=1.08)")
    parser.add_argument("-v", "--beta",  type=float, default=2.0, help="parameter beta of the heavy tailed mutation (default=2.0)")

    parser.add_argument("-b", "--start", type=int, help="start the population at a fixed number of leading ones (if this is specified)")
    parser.add_argument("-e", "--maxeval", type=float, default=2e5, help="maximal number of evaluations allowed (default=2e5)")
    
    parser.add_argument("-lex", "--lexic", action="store_true", help="enable lexicographical objective (if this is set)")
    parser.add_argument("-s", "--seed", type=int, help="use a specific random seed (if this is specified)")
    
    args = parser.parse_args()
    
    if args.input is None:
        exec_and_quit("ERR: no input file was specified, please check the usage below!\n", 
                      -1, parser.print_help)

    if args.run not in ["tourn","comma","lina","expa","powa","commai","powi","plus"]:
        exec_and_quit("ERR: unknown alorithm, please check the usage below!\n", 
                      -1, parser.print_help) 
    
    if args.run=="lina" and (args.eta>2 or args.eta<1):
        exec_and_quit("ERR: the selection pressure must be in interval [1,2] for lina!\n", 
                      -1, parser.print_help) 
    
    if args.use not in ["fixed","heavy"]:
        exec_and_quit("ERR: unknown mutation operator, please check the usage below!\n", 
                      -1, parser.print_help) 
    
    if args.tag is None: # auto tagging
        tag = "{}_{}+{}".format(args.input, 
                                args.run if args.run is not None else "tourn",
                                args.use if args.use is not None else "fixed")
                                
        if args.run is None or args.run=="tourn":
            tag += "_la{}tsize{}".format(args.la, args.tsize)
        elif args.run=="comma" or args.run=="commai" or args.run=="plus":
            tag += "_la{}mu{}".format(args.la, args.mu)
        elif args.run=="lina" or args.run=="expa":
            tag += "_la{}eta{:.2f}".format(args.la, args.eta)
        elif args.run=="powa" or args.run=="powi":
            tag += "_la{}expo{:.2f}".format(args.la, args.expo)
        
        if args.use is None or args.use=="fixed":
            tag += "chi{:.2f}".format(args.chi)
        else:
            tag += "beta{:.2f}".format(args.beta)
        
        tag += "_s{}".format(args.seed) if args.seed is not None else ""
    else:
        tag = args.tag
             
    if args.seed is not None:
        random_seed(args.seed)
                      
    data = Data()
    data.read_scp(args.input)
        
    eva = Evaluator(data, args.maxeval, lex=args.lexic) 
    
    sel = Tournament(args.tsize) # the lightest to create
    if args.run=="comma":
        sel = Comma(args.mu)
    elif args.run=="lina":
        sel = LinearAS(args.eta, args.la)
    elif args.run=="expa":
        sel = ExponentialAS(args.eta, args.la)
    elif args.run=="powa":
        sel = PowerAS(args.expo, args.la)
    elif args.run=="commai":
        sel = CommaITS(args.mu, args.la)
    elif args.run=="powi":
        sel = PowerITS(args.expo)
    
    mut = BitwiseFlip(args.chi/eva.n)
    if args.use=="heavy":
        mut = BitwiseFlipHT(args.beta, eva.n)
        
    algo = PSV(eva, sel, mut, args.la)
    if args.run=="plus":
        algo = MPL(eva, mut, args.la, args.mu)
    
    start = timer()
    if args.start is not None:
        algo.start([1]*args.start + [0]*(eva.n - args.start))
    else:
        algo.start()        
    algo.autorun()
    elapsed = timer() - start
    
    print("{} {} {} {} {:.3f} {:.3f}".format(tag, 
                         eva.bestobj,
                         "feas" if eva.bestfeas else "infeas", 
                         eva.neval2best, 
                         elapsed*eva.neval2best/args.maxeval, 
                         elapsed))
    
