/* =====================================
               Unit Testing
   ===================================== */

#include "solve_scp_libsrc.cpp"
#include <iostream>
#include <unordered_set>

void test_utility() {
    unsigned int repeat=5, n=20;
    vector<bool> bs;
    
    cout<<"random bitstring of length "<<n<<endl;
    for (unsigned int j=0; j<repeat; ++j) {
        bs = random_bstring(n); 
        for (unsigned int i=0; i<bs.size(); ++i) cout<<bs[i]; cout<<endl; 
    }        
}

void test_component() {
    SCP::Data data; 
    SCP::Model model(data);
        
    data.set_m(10); 
    data.add_subset(vector<unsigned int>({0, 1, 2, 5})); 
    data.add_subset(vector<unsigned int>({2, 4, 5})); 
    data.add_subset(vector<unsigned int>({3, 2, 8, 9})); 
    data.add_subset(vector<unsigned int>({3, 5, 6, 7})); 
    for (unsigned int i=0; i<data.N.size(); ++i) {
        cout<<"subset["<<i+1<<"]="; 
        for (unsigned int j=0; j<data.N[i].size(); ++j) {
            cout<<data.N[i][j]<<" ";
        } 
        cout<<endl;
    }
    
    model.update();
    vector<bool> s; vector<unsigned int> f; 
    
    vector<bool> s1({1, 1, 1, 0}), 
                 s2({1, 0, 1, 1}), 
                 s3({1, 1, 1, 1}); 
    vector<unsigned int> f1 = model.eval_string(s1), 
                         f2 = model.eval_string(s2), 
                         f3 = model.eval_string(s3);
    
    cout<<"model.eval("; for (unsigned int i=0; i<s1.size(); ++i) cout<<s1[i]; cout<<")=";
    cout<<"["<<f1[0]<<","<<f1[1]<<"]"<<endl;
    
    cout<<"model.eval("; for (unsigned int i=0; i<s2.size(); ++i) cout<<s2[i]; cout<<")=";
    cout<<"["<<f2[0]<<","<<f2[1]<<"]"<<endl;

    cout<<"model.eval("; for (unsigned int i=0; i<s3.size(); ++i) cout<<s3[i]; cout<<")=";
    cout<<"["<<f3[0]<<","<<f3[1]<<"]"<<endl;
    
    
    EA::Evaluator ev(data, 3);
    EA::ObjType o1 = ev(s1), o2 = ev(s2), o3 = ev(s3), o4 = ev(s3);
    
    cout<<"ev("; for (unsigned int i=0; i<s1.size(); ++i) cout<<s1[i]; cout<<")=";
    cout<<"["<<o1.lex<<",["<<o1.raw[0]<<","<<o1.raw[1]<<"],"<<o1.agg<<"]"<<endl;
    
    cout<<"ev("; for (unsigned int i=0; i<s2.size(); ++i) cout<<s2[i]; cout<<")=";
    cout<<"["<<o2.lex<<",["<<o2.raw[0]<<","<<o2.raw[1]<<"],"<<o2.agg<<"]"<<endl;
    
    cout<<"ev("; for (unsigned int i=0; i<s3.size(); ++i) cout<<s3[i]; cout<<")=";
    cout<<"["<<o3.lex<<",["<<o3.raw[0]<<","<<o3.raw[1]<<"],"<<o3.agg<<"]"<<endl;    

    cout<<"ev("; for (unsigned int i=0; i<s3.size(); ++i) cout<<s3[i]; cout<<")=";
    cout<<"["<<o4.lex<<",["<<o4.raw[0]<<","<<o4.raw[1]<<"],"<<o4.agg<<"]"<<endl;
    
    vector<EA::ObjType> popfit;
    bool popmaximize = false;
    bool poplex = false;
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({0, 1})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({2, 2})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({1, 4})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({5, 3})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({1, 0})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({0, 1})));    
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({1, 1})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({1, 0})));
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({0, 1})));    
    popfit.push_back(EA::ObjType(poplex, vector<unsigned int>({0, 2})));
    
    for (unsigned int i=0; i<popfit.size(); ++i) {
        cout<<"popfit["<<i<<"]=";
        cout<<"["<<popfit[i].lex;
        cout<<",["<<popfit[i].raw[0]<<","<<popfit[i].raw[1]<<"],";
        cout<<popfit[i].agg<<"]"<<endl;
    } 
    
    EA::Tournament optournament = EA::Tournament(2);
    EA::Comma opcomma = EA::Comma(5);
    unsigned int repeat = 5;    
    EA::Selector * opsel;
    vector<unsigned int> sel;
    
    opsel = &optournament;
    for (unsigned int j=0; j<repeat; ++j) {
        sel = (*opsel)(popfit, popmaximize);
        cout<<"tournament(popfit)=";
        for (unsigned int i=0; i<sel.size(); ++i) cout<<sel[i]<<" "; cout<<endl;
    }
    
    opsel = &opcomma; 
    for (unsigned int j=0; j<repeat; ++j) {
        sel = (*opsel)(popfit, popmaximize);
        cout<<"comma(popfit)=";
        for (unsigned int i=0; i<sel.size(); ++i) cout<<sel[i]<<" "; cout<<endl;
    }
    
    vector<bool> bs = random_bstring(40), nbs;
    EA::BitwiseFlip opmut(1.0/40);
    cout<<"bitwiseflip of"<<endl;
    for (unsigned int i=0; i<bs.size(); ++i) cout<<bs[i]; cout<<endl<<"is"<<endl; 
    for (unsigned int j=0; j<repeat; ++j) {
        nbs = opmut(bs);
        for (unsigned int i=0; i<nbs.size(); ++i) cout<<nbs[i]; cout<<endl;
    }
}

void test_algorithm() {
    
    unsigned int m = 100, n = 50, nss = 15, 
                 maxeval=200000, k=2, la = 100, mu = 33;
    double chi=0.69;
    uniform_int_distribution<unsigned int> unif(0, m-1);
    
    random_seed(1);
    
    SCP::Data data;    
    data.set_m(m);
    for (unsigned int i=0; i<n; ++i) {
        unordered_set<unsigned int> ss;
        for (unsigned int j=0; j<nss; ++j) {
            ss.insert(unif(PRNG));
        }
        data.add_subset(vector<unsigned int>(ss.begin(), ss.end()));
    }
    //for (unsigned int i=0; i<data.N.size(); ++i) {
    //    for (unsigned int j=0; j<data.N[i].size(); ++j) {
    //        cout<<data.N[i][j]<<" ";
    //    }
    //    cout<<endl;
    //}
    
    SCP::Model model(data);    
    vector<bool> randsol = random_bstring(n); 
    vector<unsigned int> obj = model.eval_string(randsol); 
    cout<<"Objective of a random solution = "<<obj[0]+obj[1];
    cout<<", feasibility = " <<(obj[0]==0?"true":"false")<<endl;
    cout<<endl;
    
    EA::Evaluator eval(data, maxeval, true); 
    EA::BitwiseFlip mut(chi/n); 
    EA::Tournament tourn(k); 
    EA::Comma comma(mu); 
        
    random_seed(clock());
    
    EA::PSV psva1(eval, tourn, mut, la);
    psva1.start();
    psva1.autorun();
    if (eval.bestobj==eval.worstobj[0]+eval.worstobj[1]) {
        cout<<"No feasible solution found by PSV-1 given "<<maxeval<<" evaluations"<<endl;
    } else {
        cout<<"Objective of the feasible solution found by PSV-1 = "<<eval.bestobj<<endl;
        cout<<"Solution = "; for (unsigned int i=0; i<eval.best.size(); ++i) cout<<eval.best[i]; cout<<endl;
        cout<<"Number of evaluations to find this solution = "<<eval.neval2best<<endl;
        
        vector<unsigned int> robj = model.eval_string(eval.best);
        cout<<"Re-evaluated the solution = "<<"["<<robj[0]<<","<<robj[1]<<"]"<<endl;         
    }
    cout<<endl;
    
        
    EA::PSV psva2(eval, comma, mut, la);
    psva2.start();
    psva2.autorun();
    if (eval.bestobj==eval.worstobj[0]+eval.worstobj[1]) {
        cout<<"No feasible solution found by PSV-2 given "<<maxeval<<" evaluations"<<endl;
    } else {
        cout<<"Objective of the feasible solution found by PSV-2 = "<<eval.bestobj<<endl;
        cout<<"Solution = "; for (unsigned int i=0; i<eval.best.size(); ++i) cout<<eval.best[i]; cout<<endl;
        cout<<"Number of evaluations to find this solution = "<<eval.neval2best<<endl;
        
        vector<unsigned int> robj = model.eval_string(eval.best);
        cout<<"Re-evaluated the solution = "<<"["<<robj[0]<<","<<robj[1]<<"]"<<endl;         
    }
    cout<<endl;
    
    EA::MPL mpl(eval, mut, la, mu);
    mpl.start();
    mpl.autorun();
    if (eval.bestobj==eval.worstobj[0]+eval.worstobj[1]) {
        cout<<"No feasible solution found by (mu+lambda)-EA given "<<maxeval<<" evaluations"<<endl;
    } else {
        cout<<"Objective of the feasible solution found by (mu+lambda)-EA = "<<eval.bestobj<<endl;
        cout<<"Solution = "; for (unsigned int i=0; i<eval.best.size(); ++i) cout<<eval.best[i]; cout<<endl;
        cout<<"Number of evaluations to find this solution = "<<eval.neval2best<<endl;
        
        vector<unsigned int> robj = model.eval_string(eval.best);
        cout<<"Re-evaluated the solution = "<<"["<<robj[0]<<","<<robj[1]<<"]"<<endl;         
    }
}

int main() {
    // random_seed(0);
        
    test_utility(); cout<<endl;
    
    test_component(); cout<<endl;
    
    test_algorithm();
        
    return 0;
    
}
