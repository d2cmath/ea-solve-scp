Experimental results and supplementary materials for the AAAI'2021 paper:

D.-C. Dang, A. Eremeev, P. K. Lehre. Escaping Local Optima with Non-Elitist 
Evolutionary Algorithms. (To appear in) Proceedings of AAAI 2021. 

~~~~
  data.tar.gz               Full unicost SCP+STS benchmark in ORLIB format
  paper-appendix.pdf        Appendix of the paper
  experimental-results.pdf  Detailed description of the experiments and results 
  extra/                    Scripts to generate GNU Parallel jobs and the plots
  result/                   Raw and analysed results and figures

