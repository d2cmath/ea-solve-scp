#!/usr/bin/env python3

# number of trials, if more than 100 trials are needed, then add more seeds below
ntrial = 40

# 100 random seeds collected from www.random.org
seed = [6502, 5918, 8508, 908, 8253, 7757, 81, 3615, 1738, 348, 3175, 6627, 
        4305, 8398, 4056, 8607, 8005, 92, 5536, 664, 845, 9820, 618, 9652, 
        5465, 6106, 7512, 4477, 4782, 7015, 5446, 2286, 7940, 9707, 1859, 
        2816, 4133, 386, 5908, 2254, 9515, 4460, 858, 6575, 9179, 8272, 2601, 
        6315, 8487, 3035, 6965, 1890, 7981, 9543, 9307, 2452, 6912, 2785, 3143, 
        3469, 941, 1063, 2674, 8433, 3836, 4869, 6184, 7486, 8715, 7682, 5345, 
        6313, 8332, 2219, 7461, 1897, 6504, 2696, 9245, 2379, 3295, 9313, 4989, 
        7022, 6573, 6880, 3108, 4345, 279, 2490, 3941, 5753, 4657, 2105, 2882, 
        7405, 3777, 6675, 404, 4209]

# list of instances to be test
instance = ["scpcyc06.txt"]

# maximal number of evaluations (preferred in string format)
maxeval = "2e8"

# list of algorithms to be test, the keys will be the firt column in the tag
algo = { 
         "tourn2"      : "./solve_scp.py -r tourn -l 200 -k 2",    # 2-tournament, lambda=200
         "tourn3"      : "./solve_scp.py -r tourn -l 200 -k 3",    # 3-tournament, lambda=200
#         "100comma200" : "./solve_scp.py -r comma -m 100 -l 200",  # (100,200) EA
#         "200plus1"    : "./solve_scp.py -r plus -m 200 -l 1",     # (200+1) EA
         "1plus1"      : "./solve_scp.py -r plus -m 1 -m 1"        # (1+1) EA
       } # basic calls of the algorithms

# list of chi to be tested
chi = [0.34, 0.45, 0.56, 0.67, 0.78, 0.89, 1.0, 1.08, 1.18, 1.28, 1.38] 
# ? not sure how to generate this list as they do not space evently, 
#   perhaps [0.34+i*0.11 for i in range(6)] + [1.0] + [1.08+i*0.10 for i in range(4)], which is long :)

# -- start to generate the job list here --
for name in algo:
    for inst in instance:
        for x in chi:
            for i in range(ntrial):
                xstr = "{:.2f}".format(x)
                seedstr = "{}".format(seed[i])
                tag = name+" "+inst+" "+xstr+" "+seedstr
                
                print("{} -i {} -t \"{}\" -e {} -x {} -s {}".format(algo[name], inst, tag, 
                                                                    maxeval, xstr, seedstr))
