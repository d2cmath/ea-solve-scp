#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt

# data source of the plot
outputfile = "output.txt"

# algorithm tags to display names, if not defined then display the raw tag
algo2name = { "1plus1"      : r"($1$+$1$) EA", 
              "tourn2"      : r"$2$-tournament", 
              "tourn3"      : r"$3$-tournament",           
              "66comma200"  : r"($66$,$200$) EA",
              "100comma200" : r"($100$,$200$) EA",
              "66plus200"   : r"($66$+$200$) EA",
              "100plus200"  : r"($100$+$200$) EA" 
}

# algorithm tags to be excluded from the plotting
algo2exclude = []

# preferred ordering of the algorithms
algoinorder = [ "1plus1", 
                "100plus200", "66plus200", 
                "100comma200", "66comma200", 
                "tourn2", "tourn3" ]

# instance name s to be excluded from the plotting
inst2exclude = []

# preferred ordering of the instances
instinorder = [ "scpcyc06", "scpcyc07", 
                "scpclr10", "scpclr11",
                "stein81", "stein135" ]

# column numbering from the current tagging scheme
COL = {
    "algo"      : 0, 
    "instance"  : 1,
    "chi"       : 2,
    "objective" : 4
}

# creating PDF instead of displaying on screen
createpng = False

# font size setting of the plot in case they are too big or too small
fontsize = 6

# --- load the data ---
data = pd.read_csv(outputfile, sep=" ", header=None, 
                   usecols=COL.values(), na_values="None").dropna()
data.columns = COL.keys()

# count the number of instances and algos in the data source
ninst = data["instance"].nunique() - len(inst2exclude)
nalgo = data["algo"].nunique() - len(algo2exclude)

# make the appropriate ordering structure
if algoinorder:
    for algo in algo2exclude: algoinorder.remove(algo)
    algoinorder = { algo:i for i, algo in enumerate(algoinorder) }

if instinorder:
    for inst in inst2exclude: instinorder.remove(inst)
    instinorder = { inst:i for i, inst in enumerate(instinorder) }

# -- start plotting ---
plt.rcParams.update({'font.size': 6})

fig, ax = plt.subplots(ninst, nalgo, sharey="row")
fig.tight_layout()

i = 0
for instalgo, instalgodata in data.groupby(["instance", "algo"]):
    inst, algo = instalgo

    inst = inst.replace(".txt", "")
    if (algo in algo2exclude) or (inst in inst2exclude): continue
    
    ialgo = algoinorder[algo] if algoinorder else i%nalgo
    iinst = instinorder[inst] if instinorder else i//nalgo
    
    plotdata = instalgodata.groupby(["chi"])["objective"].apply(list)
    
    iax = (iinst, ialgo) if ninst>1 else ialgo
    ax[iax].boxplot(x=plotdata.values, labels=plotdata.index,
                    notch=False, showfliers=True, patch_artist=True)

    if algo in algo2name: algo = algo2name[algo] 
    ax[iax].set_title(algo + " on " + inst) 

    if ialgo==0: ax[iax].set_ylabel("objective") # only to the left as they share y-axis
    if iinst+1==ninst: ax[iax].set_xlabel(r"$\chi$") # only in the bottom row to save space

    ax[iax].tick_params("x", labelrotation=45) # in case they cramp 
   
    i += 1

if createpng:
    plt.savefig(outputfile + ".png", dpi=300)
    plt.close()
else:
    plt.show()
