#!/usr/bin/env python3

import pandas as pd

# data source of the plot
outputfile = "output.txt"

# separator character for the output
sep = ","

# add the index column or not
index = False

# force conversion 
convert = None # "int64"

# column numbering from the current tagging scheme
COL = {
    "algo"      : 0, 
    "instance"  : 1,
    "chi"       : 2,
    "objective" : 4
}

# load the data
data = pd.read_csv(outputfile, sep=" ", header=None, 
                   usecols=COL.values(), na_values="None").dropna()
data.columns = COL.keys()

# -- start extracting ---
for instalgo, instalgodata in data.groupby(["instance", "algo"]):
    inst, algo = instalgo
    inst = inst.replace(".txt", "")
    
    newframe = pd.DataFrame()
    for chi, chidata in instalgodata.groupby("chi"):
        newframe.insert(len(newframe.columns), chi, 
                        pd.Series(list(chidata["objective"])) if convert is None 
                        else pd.Series(list(chidata["objective"])).astype(dtype=convert) )

    newframe.to_csv(inst+"_"+algo+".csv", sep=sep, index=index)
    
