         Pure-EA solver library for the Set Covering Problem (SCP)
           written in C++ with Python 3 interface via Cython 
                                 
    by Duc Cuong Dang, Anton Eremeev, Per Kristian Lehre, Xiaoyu Qin


# File list 

~~~~
  README.md                   This file 
  Makefile                    Makefile for compilation  
  scpcyc06.txt                An example of SCP input file 

source/                       Source code 
  solve_scp.py                Main program in Python 
  
  setup.py                    Cython setup file to build the extension 
  solve_scp_lib.pyx           Cython object wrappers/binders for C++ components 
  
  solve_scp_libsrc.pxd        Cython declaration of C++ components 
  solve_scp_libsrc.h          C++ header of core components 
  solve_scp_libsrc.cpp        C++ source code of the components 
  
  solve_scp_libsrc_test.cpp   Debug and testing program for C++ components 
  
source/prototype
  solve_scp.py                Old prototype in Python, does not have all features
  
build/                        Build folder 
  Makefile                    Local Makefile for compilation with GNU GCC
  
patch/                        Patches for Python 2 support 
  solve_scp.py.python2,patch
  Makefile.python2.patch

result-aaai2021/              Supplementary materials for our AAAI's paper

result-gecco2022/             Results of newly implemented algorithms
~~~~

# Updates

  - GECCO2022: More operators are implemented, including 
  the power-law ranking selection from (Dang et al, 2022) 
  and the heavy-tailed mutation (Doerr et al, 2017). 
  We reproduce the expriments of the AAAI's paper but now with 100 replications,
  and the best solution is always reported, even the infeasible one (since for 
  unicost SCP, there always exists a feasible one at least as good). 
  

# Compilation 

## General requirement

 - Python and Cython
 - GCC-compatible compiler supporting C++14 standard

Some tweaking is possible (see below).

## Option 1: Python+Cython version 3

To compile the library run: 
~~~~
make all 
~~~~
or 
~~~~
make python3 
~~~~
this will compile the library for Python 3 and create files
~~~~
solve_scp_lib-<cython-version>-<architect>.so
solve_scp  
~~~~
which can be used to solve SCP instances. 

## Option 2: Python+Cython version 2 

If your system only has version 2, then instead run
~~~~
make python2
~~~~

## Option 3: Python-only on a remote machine (ie. a server)

If the remote system (ie. a server) that you want to run the program/library on 
has only Python, it is still possible to compile the program on that system but
first some source code file must be generated on a local system (eg. your own 
computer) with Cython installed.

On the local system run:
~~~~
make python3
make clean-obj
~~~~

(alternatively `make python2` can be used instead of `make python3`)

Then upload the whole folder to the remote system.

On the remote system run:
~~~~
make compile
~~~~

## Option 4: Local development with distutils

If you intend to modify the code to suit your project, it is recommended to 
compile everything in the `source` folder directly. This can be done with
~~~~
python3 setup.py build_ext -i
~~~~


# Usage and examples

## The main program

Running 
~~~~ 
./solve_scp -i scpcyc06.txt 
~~~~
will solve "scpcyc06.txt" instance with the default algorithm and parameters. 

To see the available options and the output format run: 
~~~~
./solve_scp --help 
~~~~

The supported algorithms, which can be set with `-r` option, are: 

 - `tourn`: non-elitist EA with tournament selection (change its parameter with `-k` option) 
 - `comma`: non-elitist EA with (mu, lambda)-selection 
 - `lina`: non-elitist EA with linear ranking selection (change its parameter with `-n` option)  
 - `expa`: non-elitist EA with exponential ranking selection (change its parameter with `-n` option)  
 - `powa`: non-elitist EA with power-law ranking selection (change its parameter with `-c` option) 
 - `commai`: non-elitist EA with (mu, lambda)-selection using the inverse
 transform sampling method, this has more stochasticity in one generation 
 compared to the traditional comma selection 
 - `powi`: non-elitist EA with power-law ranking selection using the
 inverse transform sampling method 
 - `plus`: the elitist (mu+lambda) EA algorithm 

We note that `powa` and `powi` are the same algorithm, only implemented 
differently. On the other hand, `comma` and `commai` can behave differently 
since they are based on two views of the same principle of truncation. 

The supported mutation operator, which can be set with `-u` option, are: 

 - `fixed`: standard bitwise mutation with fixed mutation rate chi/n (change chi with `-x` option) 
 - `heavy`: heavy tailed bitwise mutation with power law (change the exponent with `-v` option) 

## The library

The library can be used in Python interpreter: 
~~~~
from solve_scp_lib import Data, Evaluator, BitwiseFlip, MPL 
data = Data() 
data.read_scp("scpcyc06.txt") 
eva = Evaluator(data, 200000) 
mut = BitwiseFlip(1.0/eva.n) 
algo = MPL(eva, mut, 1, 1) 
algo.start() 
algo.autorun() 
~~~~
These lines run the (1+1)-EA on "scpcyc06.txt" instance with 200000 calls 
to evaluation. 

Then running 
~~~~
eva.bestobj 
eva.best[:10] 
~~~~
will show the objective of the best solution found and its first 10 bits. 

The library contains the following function and classes:

 - randon_seed(): to change the random seed of the PRNG 
 - Data: to store the problem data 
 - Evaluator: to store the problem solutions as bitstring and evaluate them 
 - Tournament: tournament selection 
 - Comma: comma selection 
 - LinearAS: linear ranking selection implemented with the alias method 
 - ExponentialAS: exponential ranking selection implemented with the alias method 
 - PowerAS: power-law ranking selection mechanism implemented with the alias method 
 - CommaITS: comma selection implemented with inverse transform sampling,
 this has more stochasticity 
 - PowerITS: power-law ranking selection mechanism implemented with inverse
 transform sampling
 - BitwiseFlip: the bitwise flip mutation operator with a fixed mutation rate 
 - BitwiseFlipHT: the bitwise flip mutation operator where the mutation rate
 is chosen by a power law (eg. see (Doerr et al., 2017))
 - PSV: the population-based selection-variation algorithm 
 - MPL: the (mu+lambda) evolutionary algorithm 

# Notes

## C++ standard

The minimum requirement of the language standard is C++11 in order to support 
*lambda expressions*. Thus it is possible to lower the default C++14 to C++11 
by modifying `OPS` variable in `build/Makefile`. Likewise it can also be raised 
to higher standards, eg. C++17 or C++20, but these have not been tested.

## The prototype 

`solve_scp.py` in `source/prototype` folder was written fully in Python 3. 
This is old, slow and does not have all the algorithms.

## Related publications

D.-C. Dang, A. Eremeev, P. K. Lehre, X. Qin. Fast Non-elitist Evolutionary 
Algorithms with Power-law Ranking Selection. (To appear in) Proceeding of 
GECCO 2022. 

D.-C. Dang, A. Eremeev, P. K. Lehre. Escaping Local Optima with Non-Elitist 
Evolutionary Algorithms. Proceedings of AAAI 2021, pages 12275-12283. 

B. Doerr, H. P. Le, R. Makhmara, T. D. Nguyen. Fast Genetic Algorithms. 
Proceedings of GECCO 2017, pages 777-784. 

